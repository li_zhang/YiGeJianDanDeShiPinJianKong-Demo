#if 0
#include "opencv2/opencv.hpp"
#include "Nalu2BGR.h"
#include "H264.h"

int main(int argc, char* argv[])
{
	OpenBitstreamFile("./myData.h264");
	NALU_t *nal;
	char fName[300];
	int Frame = 0;
	nal = AllocNALU(8000000);//为结构体nalu_t及其成员buf分配空间。返回值为指向nalu_t存储空间的指针

	Nalu2BGR_Info * pNalu2BGR_Info = H264_Init();

	unsigned char *outputbuf = (unsigned char *)calloc(1000 * 1000 * 3, sizeof(char));
	unsigned int outsize = 0;

	unsigned char *m_pData = (unsigned char *)calloc(1000 * 1000 * 3, sizeof(char));

	int sizeHeBing = 0;
	while (!feof(getFile()))
	{
		GetAnnexbNALU(nal);//每执行一次，文件的指针指向本次找到的NALU的末尾，
						   //下一个位置即为下个NALU的起始码0x000001
		dump(nal);//输出NALU长度和TYPE

		sprintf(fName, "dump[Len=%d][%d].txt", nal->len, Frame);

		memset(m_pData, 0, 4);
		m_pData[3] = 1;
		memcpy(m_pData + 4, nal->buf, nal->len);
		sizeHeBing = nal->len + 4;
		Frame++;

		int ret = H264_2_RGB(pNalu2BGR_Info, (char *)m_pData, sizeHeBing, outputbuf, &outsize);
		if (ret != 0)
			continue;

		cv::Mat  image = cv::Mat(pNalu2BGR_Info->pCodecCtx->height, pNalu2BGR_Info->pCodecCtx->width, CV_8UC3);

		memcpy(image.data, outputbuf, pNalu2BGR_Info->pCodecCtx->height * pNalu2BGR_Info->pCodecCtx->width * 3);

		cv::imshow("xxx", image);

		cv::waitKey(150);

	}

	FreeNALU(nal);
	return 0;
}
#endif


#if 1
#include "Ffmpeg_Decode.h"

long consumptiontime;
long playframecount = 0;

void main()
{
	Ffmpeg_Decoder ffmpegobj;
	ffmpegobj.Ffmpeg_Decoder_Init();//初始化解码器,参数为待解码图像的宽高
	FILE *pf = NULL;
	//fopen_s(&pf, "myData.h264", "rb");
	fopen_s(&pf, "src01.h264", "rb");
	consumptiontime = clock();
	//保存带解码数据

	while (true)
	{
		ffmpegobj.nDataLen = fread(ffmpegobj.filebuf, 1, 999, pf);//读取文件数据
		if (ffmpegobj.nDataLen <= 0)
		{
			fclose(pf);
			break;
		}
		else
		{
			ffmpegobj.haveread = 0;
			while (ffmpegobj.nDataLen > 0)
			{
				int nLength = av_parser_parse2(ffmpegobj.avParserContext, ffmpegobj.c, &ffmpegobj.yuv_buff,
					&ffmpegobj.nOutSize, ffmpegobj.filebuf + ffmpegobj.haveread, ffmpegobj.nDataLen, 0, 0, 0);//查找帧头
				ffmpegobj.nDataLen -= nLength;//查找过后指针移位标志
				ffmpegobj.haveread += nLength;
				if (ffmpegobj.nOutSize <= 0)
				{

					break;
				}
				ffmpegobj.avpkt.size = ffmpegobj.nOutSize;//将帧数据放进包中
				ffmpegobj.avpkt.data = ffmpegobj.yuv_buff;
				playframecount++;

				ffmpegobj.decodelen = avcodec_decode_video2(ffmpegobj.c, ffmpegobj.m_pYUVFrame, &ffmpegobj.piclen, &ffmpegobj.avpkt);//解码
				if (ffmpegobj.decodelen < 0)
				{
					printf("error\n");
					break;
				}
				if (ffmpegobj.piclen)
				{
					if (ffmpegobj.finishInitScxt == false)//初始化格式转换函数  
					{
						ffmpegobj.finishInitScxt = true;
						ffmpegobj.scxt = sws_getContext(ffmpegobj.c->width, ffmpegobj.c->height, ffmpegobj.c->pix_fmt,
							ffmpegobj.c->width, ffmpegobj.c->height, AV_PIX_FMT_BGR24, SWS_POINT, NULL, NULL, NULL);

						int width = ffmpegobj.c->width;
						int height = ffmpegobj.c->height;
						ffmpegobj.yuv_buff = new uint8_t[width * height * 2];//初始化YUV图像数据区
						ffmpegobj.rgb_buff = new uint8_t[width * height * 3];//初始化RGB图像帧数据区
						ffmpegobj.data[0] = ffmpegobj.rgb_buff;
						ffmpegobj.linesize[0] = width * 3;
					}
					if (ffmpegobj.scxt != NULL)
					{
						//YUV转rgb  
						sws_scale(ffmpegobj.scxt, ffmpegobj.m_pYUVFrame->data, ffmpegobj.m_pYUVFrame->linesize, 0,
							ffmpegobj.c->height, ffmpegobj.data, ffmpegobj.linesize);
						ffmpegobj.Ffmpeg_Decoder_Show((char*)ffmpegobj.rgb_buff, ffmpegobj.c->width, ffmpegobj.c->height);//解码图像显示   	
					}
				}
			}
		}
	}
	printf("消耗时间%dms\n", clock() - consumptiontime);//消耗时间计算
	printf("平均1s解码帧数%lf\n", playframecount / ((clock() - consumptiontime) / (double)1000));//消耗时间计算
	printf("解码帧数%d\n", playframecount);
	getchar();

	ffmpegobj.Ffmpeg_Decoder_Close();//关闭解码器
}
#endif