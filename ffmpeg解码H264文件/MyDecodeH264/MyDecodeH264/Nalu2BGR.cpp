#include "Nalu2BGR.h"

Nalu2BGR_Info * H264_Init(void)
{
	Nalu2BGR_Info * pNalu2BGR_Info = (Nalu2BGR_Info *)calloc(1, sizeof(Nalu2BGR_Info));
	/* register all the codecs */
	avcodec_register_all();

	/* find the h264 video decoder */
	pNalu2BGR_Info->pCodec = avcodec_find_decoder(AV_CODEC_ID_H264);
	if (!pNalu2BGR_Info->pCodec) {
		fprintf(stderr, "codec not found\n");
	}
	pNalu2BGR_Info->pCodecCtx = avcodec_alloc_context3(pNalu2BGR_Info->pCodec);

	//初始化参数，下面的参数应该由具体的业务决定  
	//pNalu2BGR_Info->pCodecCtx->time_base.num = 1;
	//pNalu2BGR_Info->pCodecCtx->frame_number = 1; //每包一个视频帧  
	pNalu2BGR_Info->pCodecCtx->codec_type = AVMEDIA_TYPE_VIDEO;
	
	//pNalu2BGR_Info->pCodecCtx->bit_rate = 0;
	//pNalu2BGR_Info->pCodecCtx->time_base.den = 25;//帧率  
	//pNalu2BGR_Info->pCodecCtx->width = 640;//视频宽  
	//pNalu2BGR_Info->pCodecCtx->height = 480;//视频高  

	/* open the coderc */
	if (avcodec_open2(pNalu2BGR_Info->pCodecCtx, pNalu2BGR_Info->pCodec, NULL) < 0) {
		fprintf(stderr, "could not open codec\n");
	}
	// Allocate video frame  
	//pNalu2BGR_Info->pFrame = new AVFrame[640 * 480 * 3];
	pNalu2BGR_Info->pFrame = av_frame_alloc();
	if (pNalu2BGR_Info->pFrame == NULL)
		return NULL;
	// Allocate an AVFrame structure  
	//pNalu2BGR_Info->pFrameBGR = new AVFrame[640 * 480 * 3];
	pNalu2BGR_Info->pFrameBGR = av_frame_alloc();
	if (pNalu2BGR_Info->pFrameBGR == NULL)
		return NULL;
	pNalu2BGR_Info->first = 0;
	pNalu2BGR_Info->outBuffer = NULL;
	return pNalu2BGR_Info;

}

int H264_2_RGB(Nalu2BGR_Info * pNalu2BGR_Info, char *inputbuf, int frame_size, unsigned char *outputbuf, unsigned int*outsize)
{
	uint8_t         *buffer = NULL;

	AVCodec         *pCodec = pNalu2BGR_Info->pCodec;
	AVCodecContext  *pCodecCtx = pNalu2BGR_Info->pCodecCtx;
	
	AVFrame         *pFrame = pNalu2BGR_Info->pFrame;
	AVFrame         *pFrameBGR = pNalu2BGR_Info->pFrameBGR;

	printf("Video decoding\n");
	int ret, got_picture;
	AVPacket packet;
	av_init_packet(&packet);
	packet.size = frame_size;
	packet.data = (uint8_t *)inputbuf;

	ret = avcodec_decode_video2(pCodecCtx, pFrame, &got_picture, &packet);
	if (ret < 0)
	{
		printf("Decode Error. ret = %d（解码错误）\n", ret);
		return -1;
	}
	if (pNalu2BGR_Info->first == 0)
	{
		pNalu2BGR_Info->outBuffer = (uint8_t *)av_malloc(avpicture_get_size(AV_PIX_FMT_BGR24, pCodecCtx->width, pCodecCtx->height));
		avpicture_fill((AVPicture *)pFrameBGR, pNalu2BGR_Info->outBuffer, AV_PIX_FMT_BGR24, pCodecCtx->width, pCodecCtx->height);
		pNalu2BGR_Info->first = 1;
		pNalu2BGR_Info->img_convert_ctx = sws_getContext(pCodecCtx->width, pCodecCtx->height, pCodecCtx->pix_fmt, pCodecCtx->width, pCodecCtx->height, AV_PIX_FMT_BGR24, SWS_BICUBIC, NULL, NULL, NULL);

	}
	sws_scale(pNalu2BGR_Info->img_convert_ctx, (const uint8_t* const*)pFrame->data, pFrame->linesize, 0, pCodecCtx->height, pFrameBGR->data, pFrameBGR->linesize);
	memcpy(outputbuf, pFrameBGR->data[0], pCodecCtx->width * pCodecCtx->height * 3);
	*outsize = pCodecCtx->width * pCodecCtx->height * 3;

	return 0;
}
void H264_Release(Nalu2BGR_Info * pNalu2BGR_Info)
{
	avcodec_close(pNalu2BGR_Info->pCodecCtx);
	av_free(pNalu2BGR_Info->pCodecCtx);
	av_free(pNalu2BGR_Info->pFrame);
	av_free(pNalu2BGR_Info->pFrameBGR);
	free(pNalu2BGR_Info->outBuffer);
}
