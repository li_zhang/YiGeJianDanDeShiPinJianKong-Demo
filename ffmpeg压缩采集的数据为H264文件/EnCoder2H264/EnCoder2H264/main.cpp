#include "opencv2/opencv.hpp"
#include "MyEncoder.h"
using namespace cv;

int main()
{
	cv::VideoCapture capture(0); // 打开摄像头
	cv::Mat imageTemp;
	capture >> imageTemp;

	MyEncoder myencoder;
	myencoder.Ffmpeg_Encoder_Init();//初始化编码器  
	myencoder.Ffmpeg_Encoder_Setpara(AV_CODEC_ID_H264, imageTemp.cols, imageTemp.rows);//设置编码器参数  
																			   //图象编码  
	FILE *f = NULL;
	char * filename = "myData.h264";
	fopen_s(&f, filename, "wb");//打开文件存储编码完成数据  

	for (int i = 0; i < 100; i++)
	{
		capture >> imageTemp;
		myencoder.Ffmpeg_Encoder_Encode(f, (uchar*)imageTemp.data);//编码
		imshow("send", imageTemp);
		waitKey(100);
	}
	//myencoder.Ffmpeg_Encoder_Close();
	return 0;
}
