
#include <WINSOCK2.H>  
#include <STDIO.H>  
#include "opencv2/opencv.hpp"
#include "MyEncoder.h"
#pragma  comment(lib,"ws2_32.lib")  

using namespace cv;
typedef struct socket_info
{
	SOCKET sclient;
	sockaddr_in sin;
	int len;

}Socket_Udp_Client_Info;

Socket_Udp_Client_Info * initUdpClient()
{
	Socket_Udp_Client_Info * mSocketInfo = (Socket_Udp_Client_Info *)malloc(sizeof(Socket_Udp_Client_Info));

	WORD socketVersion = MAKEWORD(2, 2);
	WSADATA wsaData;
	if (WSAStartup(socketVersion, &wsaData) != 0)
	{
		return 0;
	}
	mSocketInfo->sclient = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

	mSocketInfo->sin.sin_family = AF_INET;
	mSocketInfo->sin.sin_port = htons(8888);
	mSocketInfo->sin.sin_addr.S_un.S_addr = inet_addr("127.0.0.1");
	mSocketInfo->len = sizeof(mSocketInfo->sin);

	return mSocketInfo;
}
void unitUdpClient(Socket_Udp_Client_Info * mSocketInfo)
{
	closesocket(mSocketInfo->sclient);
	WSACleanup();
}

int main()
{

	Socket_Udp_Client_Info * mSocketUdpClientInfo = initUdpClient();

	cv::VideoCapture capture(0); // 打开摄像头
	cv::Mat imageTemp;
	capture >> imageTemp;
	int info[3];
	int ret = 0;
	int flag = 0;
	info[0] = imageTemp.cols;
	info[1] = imageTemp.rows;
	info[2] = imageTemp.channels();
	// 先把宽高和通道数传过去
	
	ret = sendto(mSocketUdpClientInfo->sclient, (char *)info, sizeof(int) * 3, 0, (sockaddr *)&mSocketUdpClientInfo->sin, mSocketUdpClientInfo->len);
	if (ret == sizeof(int) * 3)
	{
		printf("client send width height channel succeed \n");
	}
	if (flag == 1)
	{
		printf("client dui fang succeed recevied \n");
	}
	MyEncoder myencoder;
	myencoder.Ffmpeg_Encoder_Init();
	myencoder.Ffmpeg_Encoder_Setpara(AV_CODEC_ID_H264, imageTemp.cols, imageTemp.rows);

	printf("Image size: [%d %d]\n", imageTemp.cols, imageTemp.rows);
	int oneUdpPacketSize = 1400;

	char * ptrAlloc = (char *)calloc(sizeof(char), 1000 * 1000 * 3);
	while (1)
	{
		capture >> imageTemp;
		// 发送的包的大小不能超过1500个字节
		
		AVPacket pkt;
		myencoder.Ffmpeg_Encoder_Encode_New((uint8_t *)(imageTemp.data), &pkt);
		if (pkt.size == 0)
		{
			continue;
		}
		memcpy(ptrAlloc, pkt.data, pkt.size);
		if (pkt.size <= oneUdpPacketSize)
		{
			ret = sendto(mSocketUdpClientInfo->sclient, (char *)ptrAlloc, pkt.size, 0, (sockaddr *)&mSocketUdpClientInfo->sin, mSocketUdpClientInfo->len);
			printf("ret = %d \n", ret);
		}
		else 
		{

			int numCount = pkt.size / oneUdpPacketSize;
			printf("numCont = %d \n", numCount);
			int i = 0;
			for (i = 0; i < numCount; i++)
			{
				char * ptr = (ptrAlloc) + i * oneUdpPacketSize;
				ret = sendto(mSocketUdpClientInfo->sclient, ptr, oneUdpPacketSize, 0, (sockaddr *)&mSocketUdpClientInfo->sin, mSocketUdpClientInfo->len);
				printf("ret = %d \n", ret);
			}
			char * ptr = ptrAlloc + i * oneUdpPacketSize;
			ret = sendto(mSocketUdpClientInfo->sclient, ptr, pkt.size - numCount * oneUdpPacketSize, 0, (sockaddr *)&mSocketUdpClientInfo->sin, mSocketUdpClientInfo->len);

		}
		
		av_free_packet(&pkt);

		//ret = send(mSocketTcpClientInfo->sclient, (char *)(imageTemp.data), imageTemp.cols * imageTemp.rows * imageTemp.channels(), 0);
		printf("ret = %d \n", ret);
		imshow("send", imageTemp);
		cv::waitKey(30);
	}
	return 0;
}